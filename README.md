## Rclone Backups
I created this repository to have a quick and easy way to set up backups on my devices.
I didn't develop anything new, I just added a script template to run [rclone_jobber](https://github.com/wolfv6/rclone_jobber "rclone_jobber") that goes along with cron.

If you want to use it, please **do not just modify it as you need, but fully understand how rclone_jobber and rclone work**, you can find documentation, tutorials and examples in the original repos. I'm not responsible for broken backups, data or devices.

