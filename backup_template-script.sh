#!/usr/bin/env sh

# Set rclone_jobber execution path
# Default is "./rclone_jobber/rclone_jobber.sh" (CHANGE IF YOU EDIT SUBDIRECTORY STRUCTURE!)
exe_dir="$(dirname $(realpath -s $0))"
jobber_path="$exe_dir/rclone_jobber/rclone_jobber.sh"

# The directory to back up (without a trailing slash)
source="/path/to/source"

# The directory to back up to (without a trailing slash)
# The script puts files to a subdirectory called "last_snapshot", destination=$dest/last_snapshot
dest="remote:path/to/backup/to"

# The directory where rclone moves old files, is one of:
#     "dated_directory" - move old files to a dated directory (an incremental backup)
#     "dated_files"     - move old files to old_files directory, and append move date to file names (an incremental backup)
#     ""                - old files are overwritten or deleted (a plain one-way sync backup)
move_old_files_to=""

# rclone options like "--filter-from=filter-rules_example.txt --checksum --log-level="INFO" --dry-run"
# DO NOT put these options: --backup-dir, --suffix, --log-file
options="--checksum --dry-run"

# Specify the job’s file name
# rclone_jobber.sh guards against job_name running again before the previous run is finished and prints job_name in warnings and log entries
# The command “$(basename $0)” will fill in the job’s file name for you (with the name of this script)
job_name="$(basename $0)"

# cron monitoring service URL to monitor failures
monitoring_URL="https://your-ping-domain.com/project-uuid"

#execute the backup
"$jobber_path" "$source" "$dest" "$move_old_files_to" "$options" "$job_name" "$monitoring_URL"

